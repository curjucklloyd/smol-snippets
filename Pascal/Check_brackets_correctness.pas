﻿program Check_brackets_correctness;

var
    input: string;
    stack: array of char; // Dynamic array of symbols (stack)
    correct: boolean;
    opened: char;

// Add an element to the tail of dynamic array
procedure push(var a: array of char; x: char);
begin
    SetLength(a, a.Length + 1);
    a[a.Length - 1] := x;
end;

// Returns the last element of dynamic array and deletes it
function pop(var a: array of char): char;
begin
    Result := a.Last;
    SetLength(a, a.Length - 1);
end;


begin
    readln(input);
    correct := True;
    SetLength(stack, 0); // Initializing empty array
    for var i := 1 to input.Length do // Iterating through symbols of input string
    begin
        if input[i] in '{([' then push(stack, input[i])
        else if input[i] in '{)}]' then 
        begin
            if stack.Length = 0 then
            begin
                correct := false;
                break
            end;
            opened := pop(stack);
            if (opened = '(') and (input[i] = ')') then continue;
            if (opened = '[') and (input[i] = ']') then continue;
            if (opened = '{') and (input[i] = '}') then continue;
            correct := false;
            break;
        end
    end;
    if correct and (stack.Length = 0) then writeln('Correct!')
    else writeln('Incorrect!');
    readln()
end.
