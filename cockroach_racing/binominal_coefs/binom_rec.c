//Считаем биноминальный коэфициент рекуррентно
//Это медленно, как ни крути

#include<stdio.h>

long long int c_rec(int k, int n);
long long int fact(int x);

long long int c_rec(int k, int n){
    if (k>n) return 0;
    else if (k==0) return 1;
    else return c_rec(k-1, n-1) + c_rec(k, n-1);
}


long long int fact(int x){
    if (x < 2)
        return 1;
    else
        return (x * fact(x - 1));
}


void main() {
    int k, n;
    long long int out;
    printf("Введите k, n: ");
    scanf("%d %d", &k,&n);
    out = c_rec(k, n);
    printf("%ld\n",out);
}
