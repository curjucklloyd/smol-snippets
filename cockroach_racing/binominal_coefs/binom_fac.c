//Считаем биноминальный коэфициент с помощью факториалов
//Быстро, но, ожидаемо, ломается, если числа слишком большие

#include<stdio.h>

long long int c_fac(int k, int n);
long long int fact(int x);


long long int c_fac(int k, int n){
    return fact(n) / (fact(n-k) * fact(k));
}


long long int fact(int x){
    if (x < 2)
        return 1;
    else
        return (x * fact(x-1));
}


void main() {
    int k, n;
    long long int out;
    printf("Введите k, n: ");
    scanf("%d %d", &k,&n);
    out = c_fac(k, n);
    printf("%ld\n",out);
}
