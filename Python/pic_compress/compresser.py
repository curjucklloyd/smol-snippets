from PIL import Image
import glob, os, threading

def compress(file, quality, pathOut):
        filename, ext = os.path.splitext(file)  # gets filename and extention separately
        im = Image.open(file)
        im.save(pathOut+os.sep+os.path.basename(file), quality=quality, optimize=True)
        



def main():
    print("You are now in: ", os.getcwd())  # result will be put in new folder here
    pathOut = os.getcwd()+os.sep+"downscaled"  # here it is
    pathIn = input('Set source pathname: ')  # gets source path
    quality = int(input('Set jpg quality (1..100): '))  # no resizing, compressing only

    if not os.path.exists(pathOut):  # Creating output directory only once
        os.mkdir(pathOut)
        
    counter = 0

# Тут отстатки от исходного однопоточного варианта
#    for file in glob.glob(pathIn+os.sep+"*.[jJ][pP]*[gG]"):  # searching for .jpg .JPG .jpeg
#        filename, ext = os.path.splitext(file)  # gets filename and extention separately
#        im = Image.open(file)
#        im.save(pathOut+os.sep+os.path.basename(file), quality=quality, optimize=True)
#        counter += 1
#
    
    for file in glob.glob(pathIn+os.sep+"*.[jJ][pP]*[gG]"):
        t = threading.Thread(target=compress, args=(file, quality, pathOut))
        t.start()
        counter += 1
    print(f"{counter} picture(s) to be compressed...")

ec = 0
    
    


