import random                              #
array_length = 0                               #работа с вводом
while array_length == 0:                        #
    try:                                   #цикл для корректного знч
        array_length = int(input('Введите количествo элементов массива\n'))        #задание длины "массива"
    except ValueError:                     #обработка ошибки типа
        print('Некорректный ввод')         #
        print('Повторите ввод')            #

array = []                                 #пустой массив
array_summ = 0                             #переменная для дальнейшего подсчёта ср.арифм.
summ = 0
k = 0
while not (array_length - k == 0):             #создание "массива" пользовательской длины
    unit = random.randint(1, 99)           #
    summ += unit                           #
    array += [unit]                        #
    k += 1                      #


print('Ваш массив:\n',array)
number_max = array.count(max(array))
if number_max > 1:
    print('В массиве ',number_max,' максимальных элементов. Изменён только первый, равный',max(array),' с индексом',array.index(max(array)))
else:
    print('В массиве один максимальный элемент, равный',max(array),' с индексом',array.index(max(array)))


average = int(summ // array_length)
array[array.index(max(array))] = average
print('Целая часть среднего арифметического элементов: ',average)

print('Ваш массив с изменённым максимальным элементом:\n',array)
array = sorted(array)
print('Ваш список после сортировки:\n',array)
