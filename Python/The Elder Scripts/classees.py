class SpecialString:
    def __init__(self, cont):
        self.cont = cont

    def __truediv__(self, other):
        line = "=" * len(other.cont)
        return "\n".join([self.cont, line, other.cont])

spam = SpecialString(str(input("введи первую строку")))
hello = SpecialString(str(input("веди первую строку")))
print(spam / hello)