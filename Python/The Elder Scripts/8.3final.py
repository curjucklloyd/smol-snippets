from math import *
# Задание 8.3
# Составить программу для табулирования функции,
# заданной рядом. Вычисление приближенного значения
# функции, и процесс табулирования оформить в виде подпрограмм.

def factorial(n): # Функция подсчёта факториала
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)

def formula(x, tochnost): # Функция высчитывающая ряд для каждого значения функции при табуляции
   # x = abs(t)
    s = 0
    element = x
    k = 3
    prec = 10 ** (-int(tochnost + 2))
    while not abs(element)<prec:
        s += element
        element = (x ** k)/factorial(k)
        k += 2
    s = round(s, tochnost)
#    if t < 0:
#        return -s
#    else:
    return s

def tabulate(x1, x2, dx):  # Собственно функция табуляции

    while x1 < (x2 + dx/2):
        y = formula(x1, tochnost)
        print(round(x1,9), ' - ', y, '   ---->   ', (exp(x1)-exp(-x1))/2.0)
        x1 += dx

print('Данная программа табулирует гиперболический синус для произвольных значений аргумента с заданной точностью и шагом')

while True:  # Безопасный ввод значений
    try:
        x1 = float(input('Начальный аргумент (float): '))
        x2 = float(input('Конечный аргумент (float): '))
        if x2<=x1:
            print('Начальное значение аргумента должно быть меньше конечного')
            raise ValueError
        tochnost = int(input('Знаков после запятой (int: 0..9): '))
        if tochnost < 0 or tochnost > 9:
            raise ValueError
        step = abs(float(input('Шаг табуляции: ')))
        if step > (x2-x1):
            print('Шаг табуляции должен быть меньше интервала табуляции')
            raise ValueError
        break
    except ValueError:
        print('Ошибка ввода, повторите')

f = tabulate(x1, x2, step)
print(f)
input('Конец программы, нажмите ENTER чтобы выйти')
exit(0)