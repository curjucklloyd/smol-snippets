class RectAngle:
    def __init__(self, cont):
        self.cont = cont
    
    def __truediv__(self, other):
        line = "=" * len(other.cont)
        return "\n".join([self.cont, line, other.cont])
    
    spam = RectAngle(str(input("введи первую строку")))
    egg = RectAngle(str(input("веди первую строку")))
    print(spam / egg)