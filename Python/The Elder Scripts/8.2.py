# Задание 8.2
# Составить программу, для вычисления матричного выражения
# для заданных квадратных матриц. Ввод и вывод матриц,
# а также выполнение действий над матрицами оформить в виде подпрограмм.
#  A = B^2+2C+(C^2-D)
import numpy as np
from math import sqrt


def beginning():  # Начальная подпрограмма
    print('Начало программы\n')
    print('Будет найдено А, значение выражения B^2+2C+(C^2-D)')
    print('Требуется ввести значения элементов матриц B, C, D вручную') #


def rang_input():  # Безопасный ввод ранга матриц, участвующих в вычислениях
    while True:
        try:
            r = int(input('Введите ранг матриц для вычисления: \n'))
            if r <= 0:
                raise ValueError
            print('Вычисления будут производиться с матрицами', r, '*', str(r) + ':')
            break
        except ValueError:
            print('Повторите ввод')
    return r


def matrix_input(x, L):  # Безопасный ввод элементов матриц B, C, D с клавиатуры
    while True:
        try:
            a = []
            print('Ввод элементов матрицы ', L)
            for i in range(x ** 2):
                a.append(float(input('Введите элемент ' + str(i+1))))
            a = np.array(a)
            a = a.reshape(x, x)
            print('Матрица', L, ':\n', a)
            print()
            return a
        except ValueError:
            print('Ошибка ввода, повторите.')


def matrix_mult(m1, m2, r):  # Подпрограмма для матричного произведения
    s = 0  # Сумма
    t = []  # Временная матрица
    m3 = []  # Результат произведения
    for z in range(0, r):
        for j in range(0, r):
            for i in range(0, r):
                s = s + m1[z][i] * m2[i][j]
            t.append(s)
            s = 0
        m3.append(t)
        t = []
    m3 = np.array(m3)
    m3 = m3.reshape(r, r)
    return m3


def result(b, c, d, r):  # Подпрограмма подсчёта результата
    a = matrix_mult(b,b,r) + 2*c + (matrix_mult(c,c,r) - d)
    return a


def ending(a):  # Конечная подпрограмма
    print('========================================')
    print('A = B^2+2C+(C^2-D) =\n', a)
    print('\nКонец программы')


beginning()
r = rang_input()
b = matrix_input(r, 'B')
c = matrix_input(r, 'C')
d = matrix_input(r, 'D')
a = result(b, c, d, r)
ending(a)
exit(0)