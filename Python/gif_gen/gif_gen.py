import glob
import os

from PIL import Image


def make_gif(frame_folder):
    frames = [Image.open(image) for image in reversed(glob.glob('.'+os.sep+"*.[jJ][pP]*[gG]"))]
    assert not not frames, "В этой папке нет жипегов!" # I'm sorry
    frame_one = frames[0]
    frame_one.save("NEW_GIF.gif", format="GIF", append_images=frames[1:],
               save_all=True, duration=len(frames)*40, loop=0)
    

if __name__ == "__main__":
    make_gif(f".{os.sep}")
    print("Гифка готова!")
